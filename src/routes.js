import React, { useState, useEffect } from 'react';
import { Image, View, Text, ActivityIndicator, TouchableOpacity } from 'react-native';
import { createAppContainer, createStackNavigator, createSwitchNavigator, createBottomTabNavigator } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import Reactotron from 'reactotron-react-native';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { logout, getCurrentAuthenticatedUser } from './actions/index'

import Icon from 'react-native-vector-icons/MaterialIcons';
import logo from './assets/logo.png';
import userNotFound from './assets/user-not-found.png';

import Login from './pages/login';
import Job from './pages/job';
import Events from './pages/events';
import Jobs from './pages/jobs';
import User from './pages/user';

const tabFooter = createBottomTabNavigator({
    Jobs: {
        screen: Jobs,
        navigationOptions: {
            tabBarLabel: 'Jobs',
            tabBarIcon: ({ focused, tintColor }) => {
                return <Icon name="work" size={25} color={tintColor} />;
            }
        },
    },
    Events: {
        screen: Events,
        navigationOptions: {
            tabBarLabel: 'Events',
            tabBarIcon: ({ focused, tintColor }) => {
                return <Icon name="event" size={25} color={tintColor} />;
            }
        },
    },
    User: {
        screen: User,
        navigationOptions: {
            tabBarLabel: 'Profile',
            tabBarIcon: ({ focused, tintColor }) => {
                return <Icon name="account-circle" size={25} color={tintColor} />;
            }
        },
    }
}, {
    tabBarOptions: {
        style: { backgroundColor: '#040B11' }
    },
    initialRouteName: "Jobs"
}
)

sair = async (props) => {
    await props.logout();
    await AsyncStorage.clear()
    props.navigation.navigate('Login')
}

const HeaderPrincipal = (props) => {

    useEffect(() => {
        props.getCurrentAuthenticatedUser()
    }, [])

    Reactotron.log('props', props)

    return (
        <TouchableOpacity onPress={() => sair(props)}>
            {props.userState.registro && !props.userState.registro.image &&
                <Image source={userNotFound} style={{ width: 40, height: 40, borderRadius: 40 / 2, marginRight: 10 }} />
            }

            {props.userState.registro && props.userState.registro.image &&
                <Image source={{ uri: props.userState.registro.image }} style={{ width: 40, height: 40, borderRadius: 40 / 2, marginRight: 10 }} PlaceholderContent={<ActivityIndicator />} />
            }
        </TouchableOpacity>
    )
}

const mapStateToProps = state => ({ loginState: state.loginReducer, userState: state.userReducer })
const mapDispatchToProps = dispatch => bindActionCreators({ logout, getCurrentAuthenticatedUser }, dispatch)
const HeaderPrincipalNavigator = connect(mapStateToProps, mapDispatchToProps)(HeaderPrincipal);

const Principal = createStackNavigator({
    Inicial: {
        screen: tabFooter,
        navigationOptions: (props) => ({
            headerStyle: {
                backgroundColor: '#f2f6f9'
            },
            headerTitle: <Image style={{ marginLeft: 10, width: 150, height: 25 }} source={logo} />,
            headerRight: <HeaderPrincipalNavigator {...props} />
        })
    },
    Job
}, { initialRouteName: "Inicial" })

const appNavigator = createSwitchNavigator({
    Login,
    Principal
}, { initialRouteName: "Login" })

export default createAppContainer(appNavigator)