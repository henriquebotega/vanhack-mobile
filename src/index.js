import React from 'react';
import Routes from './routes';

import { store } from './store'
import { Provider } from 'react-redux'

import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: componentWillReceiveProps'])
YellowBox.ignoreWarnings(['Warning: componentWillMount'])

if (__DEV__) {
    import('./config').then(() => console.log('Reactotron Configured'))
}

const App = () => <Provider store={store}><Routes /></Provider>

export default App