import axios from 'axios';

export const api = axios.create({
    baseURL: 'https://vanhack.herokuapp.com/api'
    // baseURL: 'http://10.0.3.2:3333/api'
})