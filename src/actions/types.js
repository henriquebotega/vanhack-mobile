export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAILURE = 'LOGIN_FAILURE'
export const LOGIN_LOGOUT = 'LOGIN_LOGOUT'

export const JOBS_FULL_REQUEST = 'JOBS_FULL_REQUEST'
export const JOBS_FULL_SUCCESS = 'JOBS_FULL_SUCCESS'
export const JOBS_FULL_FAILURE = 'JOBS_FULL_FAILURE'

export const JOB_REQUEST = 'JOB_REQUEST'
export const JOB_SUCCESS = 'JOB_SUCCESS'
export const JOB_FAILURE = 'JOB_FAILURE'

export const EVENTS_REQUEST = 'EVENTS_REQUEST'
export const EVENTS_SUCCESS = 'EVENTS_SUCCESS'
export const EVENTS_FAILURE = 'EVENTS_FAILURE'

export const USER_REQUEST = 'USER_REQUEST'
export const USER_SUCCESS = 'USER_SUCCESS'
export const USER_FAILURE = 'USER_FAILURE'

export const ordenarPorNumero = (campo, asc, colDados) => {
    return colDados.sort(function (item1, item2) {
        return (asc) ? item2[campo] - item1[campo] : item1[campo] - item2[campo]
    })
}

export const ordenarPorNome = (campo, asc, colDados) => {
    return colDados.sort(function (item1, item2) {
        return (asc) ? item1[campo].localeCompare(item2[campo]) : item2[campo].localeCompare(item1[campo])
    });
}