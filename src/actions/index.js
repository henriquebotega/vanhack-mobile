import {
    LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE, LOGIN_LOGOUT,
    JOBS_FULL_REQUEST, JOBS_FULL_SUCCESS, JOBS_FULL_FAILURE,
    EVENTS_REQUEST, EVENTS_SUCCESS, EVENTS_FAILURE,
    JOB_REQUEST, JOB_SUCCESS, JOB_FAILURE,
    USER_REQUEST, USER_SUCCESS, USER_FAILURE,
} from '../actions/types'

import AsyncStorage from '@react-native-community/async-storage';
import Reactotron from 'reactotron-react-native';

import { api } from '../services/api'

export const logout = () => (dispatch) => {
    AsyncStorage.removeItem("@vanhackToken")
    dispatch({ type: LOGIN_LOGOUT })
}

export const validar = (login, password) => (dispatch) => {
    dispatch({ type: LOGIN_REQUEST })

    api.post('/vanhack/login', { login, password }).then((res) => {
        dispatch({ type: LOGIN_SUCCESS, payload: res.data })
    }).catch((error) => {
        dispatch({ type: LOGIN_FAILURE, payload: error })
    })
}

export const getJobPipelineFull = () => (dispatch) => {
    dispatch({ type: JOBS_FULL_REQUEST })

    api.get('/vanhack/getJobPipelineFull').then((res) => {
        Reactotron.log(res.data.result.items)
        dispatch({ type: JOBS_FULL_SUCCESS, payload: res.data.result.items })
    }).catch((error) => {
        dispatch({ type: JOBS_FULL_FAILURE, payload: error })
    })
}

export const getEvents = () => (dispatch) => {
    dispatch({ type: EVENTS_REQUEST })

    api.get('/vanhack/getEvents').then((res) => {
        dispatch({ type: EVENTS_SUCCESS, payload: res.data.result })
    }).catch((error) => {
        dispatch({ type: EVENTS_FAILURE, payload: error })
    })
}

export const getJobByID = (id) => (dispatch) => {
    dispatch({ type: JOB_REQUEST })

    api.get('/vanhack/getJobByID/' + id).then((res) => {
        dispatch({ type: JOB_SUCCESS, payload: res.data.result })
    }).catch((error) => {
        dispatch({ type: JOB_FAILURE, payload: error })
    })
}

export const getCurrentAuthenticatedUser = () => async (dispatch) => {
    dispatch({ type: USER_REQUEST })

    const nToken = await AsyncStorage.getItem("@vanhackToken")

    if (nToken) {

        api.get('/vanhack/auth/getCurrentAuthenticatedUser', { 'headers': { 'token': nToken } }).then((res) => {
            dispatch({ type: USER_SUCCESS, payload: res.data.result })
        }).catch((error) => {
            dispatch({ type: USER_FAILURE, payload: error })
        })

    } else {

        api.get('/vanhack/auth/getTokenGoogle').then(async (resToken) => {
            try {
                await AsyncStorage.setItem('@vanhackToken', resToken.data[0].token);
            } catch (error) { }

            api.get('/vanhack/auth/getCurrentAuthenticatedUser', { 'headers': { 'token': resToken.data[0].token } }).then((res) => {
                dispatch({ type: USER_SUCCESS, payload: res.data.result })
            }).catch((error) => {
                dispatch({ type: USER_FAILURE, payload: error })
            })

        }).catch((error) => {
            dispatch({ type: USER_FAILURE, payload: error })
        })
    }

}
