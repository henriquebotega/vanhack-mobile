import { JOBS_FULL_REQUEST, JOBS_FULL_SUCCESS, JOBS_FULL_FAILURE, ordenarPorNumero } from '../actions/types'

const initialState = {
    registros: [],
    carregando: false,
    msgError: '',
    error: false
}

export const jobsReducer = (state = initialState, action) => {
    switch (action.type) {
        case JOBS_FULL_REQUEST:
            return {
                ...state,
                carregando: true,
                registros: [],
                msgError: '',
                error: false
            }

        case JOBS_FULL_SUCCESS:
            return {
                ...state,
                carregando: false,
                registros: ordenarPorNumero('id', false, action.payload),
                msgError: '',
                error: false
            }

        case JOBS_FULL_FAILURE:
            return {
                ...state,
                carregando: false,
                registros: [],
                msgError: action.payload,
                error: true
            }

        default:
            return state;
    }
}