import { combineReducers } from 'redux';

import { jobsReducer } from './jobsReducer';
import { loginReducer } from './loginReducer';
import { jobReducer } from './jobReducer';
import { userReducer } from './userReducer';
import { eventsReducer } from './eventsReducer';

export const rootReducer = combineReducers({
    loginReducer, jobsReducer, eventsReducer, jobReducer, userReducer
});