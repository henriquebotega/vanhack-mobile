import { JOB_REQUEST, JOB_SUCCESS, JOB_FAILURE, ordenarPorNumero } from '../actions/types'

const initialState = {
    registro: {},
    carregando: false,
    msgError: '',
    error: false
}

export const jobReducer = (state = initialState, action) => {
    switch (action.type) {
        case JOB_REQUEST:
            return {
                ...state,
                carregando: true,
                registro: {},
                msgError: '',
                error: false
            }

        case JOB_SUCCESS:
            return {
                ...state,
                carregando: false,
                registro: action.payload,
                msgError: '',
                error: false
            }

        case JOB_FAILURE:
            return {
                ...state,
                carregando: false,
                registro: {},
                msgError: action.payload,
                error: true
            }

        default:
            return state;
    }
}