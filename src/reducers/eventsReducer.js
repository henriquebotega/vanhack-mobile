import { EVENTS_REQUEST, EVENTS_SUCCESS, EVENTS_FAILURE, ordenarPorNumero } from '../actions/types'

const initialState = {
    registros: [],
    carregando: false,
    msgError: '',
    error: false
}

export const eventsReducer = (state = initialState, action) => {
    switch (action.type) {
        case EVENTS_REQUEST:
            return {
                ...state,
                carregando: true,
                registros: [],
                msgError: '',
                error: false
            }

        case EVENTS_SUCCESS:
            return {
                ...state,
                carregando: false,
                registros: ordenarPorNumero('id', false, action.payload),
                msgError: '',
                error: false
            }

        case EVENTS_FAILURE:
            return {
                ...state,
                carregando: false,
                registros: [],
                msgError: action.payload,
                error: true
            }

        default:
            return state;
    }
}