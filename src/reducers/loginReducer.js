import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE, LOGIN_LOGOUT } from '../actions/types'
import Reactotron from 'reactotron-react-native';

const initialState = {
    user: null,
    validando: false,
    msgError: '',
    error: false
}

export const loginReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_REQUEST:
            return {
                ...state,
                user: null,
                validando: true,
                msgError: '',
                error: false
            }

        case LOGIN_SUCCESS:
            return {
                ...state,
                user: action.payload,
                validando: false,
                msgError: '',
                error: false
            }

        case LOGIN_FAILURE:
            return {
                ...state,
                user: null,
                validando: false,
                msgError: action.payload,
                error: true
            }

        case LOGIN_LOGOUT:
            return {
                ...state,
                user: null,
                validando: false,
                msgError: '',
                error: false
            }

        default:
            return state;
    }
}