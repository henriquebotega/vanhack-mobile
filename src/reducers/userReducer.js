import { USER_REQUEST, USER_SUCCESS, USER_FAILURE, ordenarPorNumero } from '../actions/types'

const initialState = {
    registro: {},
    carregando: false,
    msgError: '',
    error: false
}

export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_REQUEST:
            return {
                ...state,
                carregando: true,
                registro: {},
                msgError: '',
                error: false
            }

        case USER_SUCCESS:
            return {
                ...state,
                carregando: false,
                registro: action.payload,
                msgError: '',
                error: false
            }

        case USER_FAILURE:
            return {
                ...state,
                carregando: false,
                registro: {},
                msgError: action.payload,
                error: true
            }

        default:
            return state;
    }
}