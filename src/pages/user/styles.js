import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    linha: {
        flex: 1,
        backgroundColor: '#f5f5f5',
        margin: 10,
        padding: 10
    },
    imagem: {
        padding: 10,
        width: 100,
        height: 100,
    },
    titulo: {
        fontSize: 16,
        fontWeight: 'bold'
    },
})

export default styles