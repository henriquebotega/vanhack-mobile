import React from 'react';
import { connect } from 'react-redux'
import Reactotron from 'reactotron-react-native';

import { View, Image, TextInput, SafeAreaView, Text, TouchableOpacity } from 'react-native';

import styles from './styles';

const User = (props) => {

    return (
        <SafeAreaView style={styles.linha}>

            {props.userState.registro.id &&
                <View>
                    <View style={{ flexDirection: 'row', marginBottom: 50 }}>
                        <Image style={styles.imagem} source={{ uri: props.userState.registro.image }} />

                        <View style={{ flexDirection: 'column', padding: 10 }}>
                            <Text style={styles.titulo}>ID</Text>
                            <Text style={styles.input}>{props.userState.registro.id.toString()}</Text>
                        </View>

                        <View style={{ flexDirection: 'column', padding: 10 }}>
                            <Text style={styles.titulo}>Name</Text>
                            <Text style={styles.input}>{props.userState.registro.firstName} {props.userState.registro.lastName}</Text>
                        </View>
                    </View>

                    <View style={{ padding: 10 }}>
                        <Text style={styles.titulo}>E-mail</Text>
                        <Text style={styles.input}>{props.userState.registro.email}</Text>
                    </View>

                    <View style={{ padding: 10 }}>
                        <Text style={styles.titulo}>Verification Process (English)</Text>
                        <Text style={styles.input}>{(props.userState.registro.verificationProcess.english) ? 'Yes' : 'No'}</Text>
                    </View>

                    <View style={{ padding: 10 }}>
                        <Text style={styles.titulo}>Can Apply For Job</Text>
                        <Text style={styles.input}>{(props.userState.registro.verificationProcess.canAppplyForJob) ? 'Yes' : 'No'}</Text>
                    </View>
                </View>
            }
        </SafeAreaView>
    )
}

const mapStateToProps = state => ({ userState: state.userReducer })
export default connect(mapStateToProps, null)(User);