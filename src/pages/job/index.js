import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { View, Alert, Text, FlatList, ScrollView, TouchableHighlight, SafeAreaView } from 'react-native';

import Reactotron from 'reactotron-react-native';

import { getJobByID } from '../../actions'

import styles from './styles';

class Job extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        headerStyle: {
            backgroundColor: '#f2f6f9'
        },
        title: navigation.getParam('item').positionName
    })

    state = {
        registro: {}
    }

    componentDidMount() {
        const item = this.props.navigation.getParam('item')
        this.props.getJobByID(item.id)
    }

    componentDidUpdate(prevProps) {
        if (this.props.jobState !== prevProps.jobState) {
            this.setState({ registro: this.props.jobState.registro })
        }
    }

    render() {
        return (
            <SafeAreaView style={styles.linha}>
                <ScrollView>
                    <Text style={styles.titulo}>{this.state.registro.positionName}</Text>

                    <View style={styles.coluna}>
                        <Text>{this.state.registro.city} - {this.state.registro.country}</Text>
                    </View>

                    <Text>{this.state.registro.description}</Text>
                    <Text>Remote: {(this.state.registro.remoteJob) ? 'Yes' : 'No'}</Text>

                    {this.state.registro.salaryRangeStart > 0 &&
                        <Text>Salary (CAD): {this.state.registro.salaryRangeStart}k</Text>
                    }

                    <View style={{ marginTop: 10 }}>
                        <View style={{ flexDirection: 'row' }}>
                            {this.state.registro.mustHaveSkills && this.state.registro.mustHaveSkills.map((obj, i) => (
                                <View key={i} style={{ margin: 3, padding: 3, borderWidth: 1, borderColor: 'orange' }}>
                                    <Text style={{ textTransform: 'capitalize' }}>{obj.name}</Text>
                                </View>
                            ))}
                        </View>

                        <View style={{ flexDirection: 'row' }}>
                            {this.state.registro.niceToHaveSkills && this.state.registro.niceToHaveSkills.map((obj, i) => (
                                <View key={i} style={{ margin: 3, padding: 3, borderWidth: 1, borderColor: 'blue' }}>
                                    <Text style={{ textTransform: 'capitalize' }}>{obj.name}</Text>
                                </View>
                            ))}
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TouchableHighlight style={styles.buttonBack} onPress={() => { this.props.navigation.navigate('Jobs') }}>
                            <Text style={styles.buttonText}>Back</Text>
                        </TouchableHighlight>

                        <View style={{ width: 10 }} />

                        <TouchableHighlight disabled={this.state.registro.applied} style={styles.buttonApply} onPress={() => { Alert.alert('Alert', "It's just a test") }}>
                            <Text style={styles.buttonText}>Apply</Text>
                        </TouchableHighlight>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
};

const mapStateToProps = state => ({ jobState: state.jobReducer })
const mapDispatchToProps = dispatch => bindActionCreators({ getJobByID }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Job);