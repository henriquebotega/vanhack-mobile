import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { View, Text, Alert, FlatList, TouchableHighlight, SafeAreaView } from 'react-native';
import Reactotron from 'reactotron-react-native';

import { getJobPipelineFull } from '../../actions'

import styles from './styles';

class Jobs extends React.Component {

    state = {
        registros: []
    }

    componentDidMount() {
        this.props.getJobPipelineFull()
    }

    componentDidUpdate(prevProps) {
        if (this.props.jobsState !== prevProps.jobsState) {
            this.setState({ registros: this.props.jobsState.registros })
        }
    }

    recortarTexto = (texto, tm) => {
        return texto.substr(0, tm || 100) + "...";
    }

    renderItem = (item, index) => {
        return (
            <View key={index} style={styles.linha}>
                <Text style={styles.titulo}>{item.positionName}</Text>

                <View style={styles.coluna}>
                    <Text>{item.city} - {item.country}</Text>
                </View>

                <Text>{this.recortarTexto(item.description, 200)}</Text>

                {item.salaryRangeStart > 0 &&
                    <Text>Salary (CAD): {item.salaryRangeStart}k</Text>
                }

                <View style={{ marginTop: 10 }}>
                    <View style={{ flexDirection: 'row' }}>
                        {item.mustHaveSkills.map((obj, i) => (
                            <View key={i} style={{ margin: 3, padding: 3, borderWidth: 1, borderColor: 'orange' }}>
                                <Text style={{ textTransform: 'capitalize' }}>{obj.name}</Text>
                            </View>
                        ))}
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                        {item.niceToHaveSkills.map((obj, i) => (
                            <View key={i} style={{ margin: 3, padding: 3, borderWidth: 1, borderColor: 'blue' }}>
                                <Text style={{ textTransform: 'capitalize' }}>{obj.name}</Text>
                            </View>
                        ))}
                    </View>
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableHighlight style={styles.buttonApply} onPress={() => { this.props.navigation.navigate('Job', { item }) }}>
                        <Text style={styles.buttonText}>Details</Text>
                    </TouchableHighlight>

                    <View style={{ width: 10 }} />

                    <TouchableHighlight disabled={item.applied} style={styles.buttonApply} onPress={() => { Alert.alert('Alert', "It's just a test") }}>
                        <Text style={styles.buttonText}>Apply</Text>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }

    render() {
        return (
            <FlatList data={this.state.registros} renderItem={({ item, index }) => this.renderItem(item, index)} keyExtractor={(item) => item.id.toString()} />
        )
    }
};

const mapStateToProps = state => ({ jobsState: state.jobsReducer })
const mapDispatchToProps = dispatch => bindActionCreators({ getJobPipelineFull }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Jobs);