import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    linha: {
        flex: 1,
        backgroundColor: '#f5f5f5',
        margin: 10,
        padding: 10
    },
    coluna: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    titulo: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    buttonApply: {
        flex: 1,
        marginTop: 10,
        height: 46,
        backgroundColor: '#0675ce',
        alignSelf: 'stretch',
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16,
    },
})

export default styles