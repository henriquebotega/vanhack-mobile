import React, { useState, useEffect } from 'react';
import { View, ToastAndroid, Alert, KeyboardAvoidingView, Platform, TextInput, ImageBackground, Image, Text, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Reactotron from 'reactotron-react-native';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { validar } from '../../actions/index'

import logo from '../../assets/logo.png';
import fundo from '../../assets/fundo.png';

import styles from './styles';

const Login = (props) => {

    const [login, setLogin] = useState('henriquebotega@gmail.com')
    const [password, setPassword] = useState('brothers158210')

    useEffect(() => {
        AsyncStorage.getItem('@loggedInVanhack').then((objAdmin) => {
            if (objAdmin) {
                props.navigation.navigate('Principal')
            }
        })
    }, [])

    useEffect(() => {
        if (props.loginState.user) {
            try {
                AsyncStorage.setItem('@loggedInVanhack', JSON.stringify(props.loginState.user))
            } catch (e) { }

            props.navigation.navigate('Principal')
        }

        if (props.loginState.error) {
            ToastAndroid.show('Os dados informados estão inválidos', ToastAndroid.LONG);
        }

    }, [props.loginState])

    entrar = () => {
        if (!login || !password) {
            ToastAndroid.show('Preencha os campos solicitados', ToastAndroid.LONG);
            return;
        }

        props.validar(login, password)
    }

    return (
        <ImageBackground source={fundo} style={{ width: '100%', height: '100%' }}>
            <KeyboardAvoidingView style={styles.container} behavior="padding" enabled={Platform.OS === 'ios'}>
                <View style={styles.logo}>
                    <Image width="20" height="20" source={logo} />
                </View>

                <TextInput style={styles.input} value={login} onChangeText={setLogin} autoCapitalize="none" autoCorrect={false} placeholderTextColor="#999" placeholder="Type your login" />

                <TextInput style={styles.input} value={password} onChangeText={setPassword} secureTextEntry={true} autoCapitalize="none" autoCorrect={false} placeholderTextColor="#999" placeholder="Type your password" />

                <TouchableOpacity disabled={props.loginState.validando} style={styles.button} onPress={() => entrar()}>
                    <Text style={styles.buttonText}>{!props.loginState.validando ? 'SIGN IN' : 'LOADING...'}</Text>
                </TouchableOpacity>

                <View style={styles.containerFooter}>
                    <TouchableOpacity onPress={() => { Alert.alert('Alert', "It's just a test") }}>
                        <Text style={styles.buttonTextFooter}>Sign up</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { Alert.alert('Alert', "It's just a test") }}>
                        <Text style={styles.buttonTextFooter} onPress={() => { Alert.alert('Alert', "It's just a test") }}>Forgot password?</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        </ImageBackground>
    )
};

const mapStateToProps = state => ({ loginState: state.loginReducer })
const mapDispatchToProps = dispatch => bindActionCreators({ validar }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Login);