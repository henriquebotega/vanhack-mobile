import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        padding: 30,
    },
    logo: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    input: {
        backgroundColor: '#fff',
        alignSelf: 'stretch',
        borderWidth: 1,
        borderColor: '#ddd',
        borderRadius: 4,
        marginTop: 10,
        paddingHorizontal: 15,
    },
    button: {
        height: 46,
        backgroundColor: '#df4723',
        alignSelf: 'stretch',
        borderRadius: 4,
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',

    },
    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16,
    },
    containerFooter: {
        marginTop: 20,
        marginBottom: 20,
        alignSelf: 'stretch',
        justifyContent: "space-between",
        flexDirection: 'row'
    },
    buttonTextFooter: {
        color: '#fff',
    },
    titulo: {
        backgroundColor: '#fff'
    }
})

export default styles