import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    linha: {
        flex: 1,
        backgroundColor: '#f5f5f5',
        padding: 5,
        marginBottom: 10
    },
    coluna: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    imagemTopo: {
        position: 'relative'
    },
    flag: {
        position: 'absolute',
        right: 10,
        bottom: -14,
        width: 30,
        height: 30,
        borderRadius: 30 / 2
    },
    tituloEvent: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#FFF',
        padding: 5,
        backgroundColor: '#A5A5A5'
    },
    titulo: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    buttonClose: {
        height: 46,
        backgroundColor: '#df4723',
        alignSelf: 'stretch',
        borderRadius: 4,
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',

    },
    buttonDetails: {
        height: 46,
        backgroundColor: '#0675ce',
        alignSelf: 'stretch',
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',

    },
    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16,
    },
})

export default styles