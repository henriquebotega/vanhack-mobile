import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { View, Image, Dimensions, ActivityIndicator, Text, FlatList, ScrollView, TouchableHighlight, SafeAreaView } from 'react-native';
import SvgUri from 'react-native-svg-uri-reborn';
import Moment from 'moment';
import Modal from "react-native-modal";
import HTML from 'react-native-render-html';

import Reactotron from 'reactotron-react-native';

import { getEvents } from '../../actions'

import styles from './styles';

class Events extends React.Component {

    state = {
        modalVisible: false,
        itemAtual: {},
        registros: []
    }

    componentDidMount() {
        this.props.getEvents()
    }

    componentDidUpdate(prevProps) {
        if (this.props.eventsState !== prevProps.eventsState) {
            this.setState({ registros: this.props.eventsState.registros })
        }
    }

    setModalVisible() {
        this.setState({ modalVisible: !this.state.modalVisible });
    }

    formatarData = (data) => {
        return Moment(data).format('MMMM Do YYYY');
    }

    renderItem = (item, index) => {
        const dimensions = Dimensions.get('window');
        const imageHeight = Math.round(dimensions.width * 9 / 16);
        const imageWidth = dimensions.width;

        return (
            <View key={index} style={styles.linha}>

                <View style={styles.imagemTopo}>
                    <Image source={{ uri: item.thumb }} style={{ width: imageWidth, height: imageHeight }} PlaceholderContent={<ActivityIndicator />} />
                    <SvgUri style={styles.flag} source={{ uri: item.flag }} />
                </View>

                <Text style={styles.titulo}>{item.name}</Text>
                <Text>{this.formatarData(item.startDate)}</Text>

                <View style={styles.coluna}>
                    <View style={{ flexDirection: 'row' }}>
                        <Image source={{ uri: 'https://webapp-vanhack-react-prod.azurewebsites.net/14b97ad63e6d7e5a8807f06f0c0b1aca.png' }} style={{ width: 20, height: 20 }} />
                        <Text>{item.city} - {item.country}</Text>
                    </View>
                    <Text>Deadline: {this.formatarData(item.endApplicationDate)}</Text>
                </View>

                <TouchableHighlight style={styles.buttonDetails} onPress={() => { this.setModalVisible(); this.setState({ itemAtual: item }) }}>
                    <Text style={styles.buttonText}>Details</Text>
                </TouchableHighlight>
            </View>
        )
    }

    render() {
        return (
            <>
                <Modal isVisible={this.state.modalVisible} animationType="slide" transparent={true} onRequestClose={() => this.setState({ itemAtual: {} })}>
                    <View style={{ flex: 1 }}>
                        <ScrollView style={{ backgroundColor: '#fff', borderRadius: 5 }}>
                            <View style={{ padding: 10 }}>
                                <View style={styles.imagemTopo}>
                                    <Image source={{ uri: this.state.itemAtual.thumb }} style={{ width: Dimensions.get('window').width - 60, height: 200 }}  PlaceholderContent={<ActivityIndicator />} />
                                </View>

                                <Text style={styles.titulo}>{this.state.itemAtual.name}</Text>
                                <Text style={{ marginBottom: 10 }}>{this.state.itemAtual.subtitle}</Text>

                                <Text style={styles.tituloEvent}>The event</Text>
                                <HTML html={this.state.itemAtual.theEvent} />

                                <Text style={styles.tituloEvent}>The candidates</Text>
                                <HTML html={this.state.itemAtual.theCandidates} />

                                <View style={styles.coluna}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text>{this.state.itemAtual.city} - {this.state.itemAtual.country}</Text>
                                    </View>

                                    <Text>Deadline: {this.formatarData(this.state.itemAtual.endApplicationDate)}</Text>
                                </View>
                            </View>
                        </ScrollView>

                        <TouchableHighlight style={styles.buttonClose} onPress={() => { this.setModalVisible(); }}>
                            <Text style={styles.buttonText}>Close</Text>
                        </TouchableHighlight>
                    </View>
                </Modal>

                <FlatList data={this.state.registros} renderItem={({ item, index }) => this.renderItem(item, index)} keyExtractor={(item) => item.id.toString()} />
            </>
        )
    }
};

const mapStateToProps = state => ({ eventsState: state.eventsReducer })
const mapDispatchToProps = dispatch => bindActionCreators({ getEvents }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Events);